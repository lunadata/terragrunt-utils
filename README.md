# Terragrunt Utils

This project contain a set of simple utilities useful when working with [terragrunt](https://terragrunt.gruntwork.io/). These utilities are pretty opionated though.

## deprecation warning

[tgwrap](./tgwrap/README.md) (see [pypi.org](pypi.org)) is a python wrapper around terragrunt. It provides the most convenience (although heavily opionated) interface. `tgwrap` supercedes this.

## aliases and functions

Terragrunt is awesome but its commands and especially options cannot be pretty verbose, as well as pretty sensitive when it comes to the right combination of [cli options](https://terragrunt.gruntwork.io/docs/reference/cli-options/).

In order to enhance productivity and reproducability, we have defined a [set of aliases](./tg-shell.sh) that can be sourced in your shell and provides a rich set of aliases and functions.

In order to use these aliases and functions, source [the file](./tg-shell.sh), e.g. from your `~/.zshrc`:

```console
source "~/git/lunadata/terragrunt-utils/tg-shell.sh"
```

After that commands such as `tga` (to run `terragrunt apply`) and `tgraa` (to run `terragrunt run-all apply`) can be run. For more info, just check [the file](./tg-shell.sh), it is pretty self-explanatory.

### Environment variables

Some of the aliases and functions above require the following environment variables to be set:

* `TERRAGRUNT_SOURCE`: (optional) the path where the local terraform configuration can be found. Note that the aliases all work with the `run-all` type of commands, so that this variable can be set to the root of your module repository.
* `TERRASAFE_CONFIG`: (optional) the path to the [terrasafe](https://pypi.org/project/terrasafe/) configuration.

## Surpressing output

All additionally generated messages are written to `stderr` (which is similar to what terragrunt does). So if you want to stick with plain terragrunt output you could redirect `stderr` to the famous `/dev/null`.
