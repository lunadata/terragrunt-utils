# terragrunt settings
#
# In general, we use run-all also for a regular commands to so that TERRAGRUNT_SOURCE setting can point to the root of the module (mono) repository.
# For a regular command, the TERRAGRUNT_SOURCE must point to the exact path of the modulea itself.
# We are using CLI options to make these commands work similar to the regular command.
#

echo "This is deprecated, use tgwrap (on pypi) instead!"

# clean terragrunt cache
alias tgclean='find . -name ".terragrunt-cache" -type d -exec rm -rf {} \; ; find . -name ".terraform" -type d -exec rm -rf {} \;'
# and now also including lock-files, this is usually not recommended!
alias tgcleanall='tgclean ; find . -name ".terraform.lock.hcl" -type f -exec rm -rf {} \;'

# some simple shortened commands
alias tg="terragrunt run-all"
alias tgi='tg init --terragrunt-ignore-external-dependencies --terragrunt-non-interactive'
alias tgiu='tgi --upgrade'
alias tgv='tg validate --terragrunt-ignore-external-dependencies --terragrunt-non-interactive'
alias tgo='tg output --terragrunt-ignore-external-dependencies --terragrunt-non-interactive'
alias tgs="tg state --terragrunt-ignore-external-dependencies --terragrunt-non-interactive"

# various forms of 'terragrunt plan'
alias tgp='tg plan --terragrunt-ignore-external-dependencies --terragrunt-non-interactive -out=planfile'
alias tgpu='tgp --terragrunt-source-update' # plan, but with a source update
alias tgpd='tgp --terragrunt-log-level debug --terragrunt-debug' # plan, but with debugging enabled

# various forms of 'terragrunt apply'
alias tga='tg apply --terragrunt-no-auto-approve --terragrunt-ignore-external-dependencies --terragrunt-non-interactive'
alias tgau='tga --terragrunt-source-update' # apply but with a source update

# terragrunt destroy
alias tgd='tg destroy --terragrunt-no-auto-approve --terragrunt-ignore-external-dependencies --terragrunt-non-interactive'

# These are the real run-all commands, which will include all dependencies
alias tgrai='tg init --terragrunt-include-external-dependencies --terragrunt-non-interactive'
alias tgrav='tg validate --terragrunt-include-external-dependencies --terragrunt-non-interactive'
alias tgrap='tg plan -out=planfile --terragrunt-include-external-dependencies --terragrunt-non-interactive'
alias tgrapu='tgrap --upgrade'
alias tgras='tg show --terragrunt-include-external-dependencies --terragrunt-non-interactive -json planfile'
alias tgraa='tg apply --terragrunt-include-external-dependencies --terragrunt-parallelism 1'
alias tgraau='tgraa --upgrade'

# This will run terrasafe, this assumes a plan (with planfile as output) has been ran previously across all projects
# terrasafe needs to be installed, see: https://pypi.org/project/terrasafe/
function tgrats {
    if [ -z "${TERRASAFE_CONFIG}" ]
        then
        echo "Then environment variable 'TERRASAFE_CONFIG' is not set!"
        kill -INT $$
    fi

    echo "Running terrasafe validation"

    all_output=$(mktemp)
    # this will output ajson line file
    tg show -json planfile --terragrunt-non-interactive --terragrunt-no-auto-init --terragrunt-parallelism 1 > ${all_output}

    # escape the \ in the json, to ensure the subsequent 'echo' will output proper(ly escaped) json
    sed -E -i '' 's|\"|\\\"|g' ${all_output}

    # run a validate for each of the (json) lines from the output
    while read line
    do
        echo ${line} | terrasafe --config ${TERRASAFE_CONFIG}
    done < ${all_output}
}

function dlz_promote {
  echo 'Deploys to next stage wihtin same domain'
  echo 'example: shared/dev/*/*.hcl -> shared/prd/*/*.hcl'

  DLZ_SOURCE_STAGE=${DLZ_SOURCE_STAGE="dev"}
  DLZ_TARGET_STAGE=${DLZ_TARGET_STAGE="prd"}

  if ! [ -d "${DLZ_SOURCE_STAGE}" ]; then
    echo "The source directory '${DLZ_SOURCE_STAGE}' cannot be found! Is this function executed from correct path?"
    kill -INT $$
  fi

  echo Promoting from $DLZ_SOURCE_STAGE to $DLZ_TARGET_STAGE
  rsync -aim --include='terragrunt.hcl' --include='.terraform.lock.hcl' --exclude env.hcl --exclude='.terragrunt-cache/' $DLZ_SOURCE_STAGE/ $DLZ_TARGET_STAGE/
}

function dlz_deploy {
  echo 'Deploy to target domain(s)'
  echo 'example domain1/dev -> domain2/dev domain3/dev domain4/dev'

  DLZ_SOURCE_STAGE=${DLZ_SOURCE_STAGE="dev"}

  if [ -z "${DLZ_SOURCE_DOMAIN}" ] || [ -z "${DLZ_SOURCE_STAGE}" ] || [ -z "${DLZ_TARGET_DOMAINS}" ]
  then
    echo "The environment variable 'DLZ_SOURCE_DOMAIN', 'DLZ_SOURCE_STAGE' or 'DLZ_TARGET_DOMAINS' is not set!"
    kill -INT $$
  fi

  if ! [ -d "${DLZ_SOURCE_DOMAIN}" ]; then
    echo "The source directory '${DLZ_SOURCE_DOMAIN}' cannot be found! Is this function executed from correct path?"
    kill -INT $$
  fi

  for DLZ_DOMAIN in ${DLZ_TARGET_DOMAINS[@]}; do
    echo Deploying from $DLZ_SOURCE_DOMAIN/$DLZ_SOURCE_STAGE to $DLZ_DOMAIN/$DLZ_SOURCE_STAGE
    rsync -aim --delete --include='terragrunt.hcl' --include='.terraform.lock.hcl' --exclude='.terragrunt-cache/'  $DLZ_SOURCE_DOMAIN/$DLZ_SOURCE_STAGE/ $DLZ_DOMAIN/$DLZ_SOURCE_STAGE/
  done
}
